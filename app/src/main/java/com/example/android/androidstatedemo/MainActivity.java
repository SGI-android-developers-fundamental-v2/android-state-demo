package com.example.android.androidstatedemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MonitorAndroidState";
    private static final String ClassName = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,ClassName+": onCreate");
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,ClassName+": onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,ClassName+": onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,ClassName+": onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,ClassName+": onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,ClassName+": onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,ClassName+": onDestroy");
    }

    public void pindahKeSecondActivity(View v){
        Intent i = new Intent(this, SecondActivity.class);
        startActivity(i);
    }

    public void pindahDanMatikanActivityIni(View v){
        this.pindahKeSecondActivity(v);
        finish();
    }
}