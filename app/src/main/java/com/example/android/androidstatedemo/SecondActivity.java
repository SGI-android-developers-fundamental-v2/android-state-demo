package com.example.android.androidstatedemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class SecondActivity extends AppCompatActivity {

    private static final String TAG = "MonitorAndroidState";
    private static final String ClassName = "SecondActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,ClassName+": onCreate");
        setContentView(R.layout.activity_second);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,ClassName+": onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,ClassName+": onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,ClassName+": onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,ClassName+": onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,ClassName+": onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,ClassName+": onDestroy");
    }

    public void finishActivityIni(View v) {
        finish();
    }

    public void keluarDariAplikasi(View v) {
        System.exit(0);
    }
}